/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "grid.h"

#define MAX_CELL_COUNT 5000000

/********************************************************************/
struct grid *grid_create(void){

  struct grid *grid;

  grid = calloc(1,sizeof(struct grid)); 

  grid->x = axis_create("x");
  grid->y = axis_create("y");
  grid->z = axis_create("z");

  return grid;
}

/********************************************************************/
void grid_mesh_block(struct grid *grid,struct block *block){

  axis_mesh_interval(grid->x,block->x,block->length,block->resolution);
  axis_mesh_interval(grid->y,block->y,block->width,block->resolution);
  axis_mesh_interval(grid->z,block->z,block->height,block->resolution);

  return;
}

/********************************************************************/
int grid_get_dimension(struct grid *grid,int *Nx,int *Ny,int *Nz){

  int nx,ny,nz;

  nx = axis_count_interval(grid->x);
  ny = axis_count_interval(grid->y);
  nz = axis_count_interval(grid->z);

  if(Nx)
    *Nx = nx;

  if(Ny)
    *Ny = ny;

  if(Nz)
    *Nz = nz;

  return nx*ny*nz;
}

/********************************************************************/
void grid_alloc_cell(struct grid *grid){

  int n;

  n = grid_get_dimension(grid,NULL,NULL,NULL);

  if(n > MAX_CELL_COUNT){
    fprintf(stderr,"Error: required more than %d cells\n",MAX_CELL_COUNT);
    exit(-1);
  }

  grid->cell_p = calloc(n,sizeof(double));
  grid->cell_t = calloc(n,sizeof(double));
  grid->cell_c = calloc(n,sizeof(double));
  grid->cell_gx = calloc(n,sizeof(double));
  grid->cell_gy = calloc(n,sizeof(double));
  grid->cell_gz = calloc(n,sizeof(double));

  return;
}

/********************************************************************/
int grid_get_cell_index(struct grid *grid,int i,int j,int k){

  int nx,ny;

  grid_get_dimension(grid,&nx,&ny,NULL);

  return k*nx*ny+j*nx+i;
}

/********************************************************************/
void grid_get_cell_dimension(struct grid *grid,int i,int j,int k,
  double *dx,double *dy,double *dz){

  *dx = axis_get_interval(grid->x,i);
  *dy = axis_get_interval(grid->y,j);
  *dz = axis_get_interval(grid->z,k);

  return;
}

/********************************************************************/
void grid_map_block(struct grid *grid,struct block *block,
  int *imin,int *jmin,int *kmin,int *imax,int *jmax,int *kmax){

  *imin = axis_get_closest_tick(grid->x,block->x);
  *jmin = axis_get_closest_tick(grid->y,block->y);
  *kmin = axis_get_closest_tick(grid->z,block->z);

  *imax = axis_get_closest_tick(grid->x,block->x+block->length);
  *jmax = axis_get_closest_tick(grid->y,block->y+block->width);
  *kmax = axis_get_closest_tick(grid->z,block->z+block->height);

  return;
}

/********************************************************************/
void grid_set_cell(struct grid *grid,int i,int j,int k,
  struct material *matl,double pvol){

  double dx,dy,dz;
  int index;

  grid_get_cell_dimension(grid,i,j,k,&dx,&dy,&dz);

  index = grid_get_cell_index(grid,i,j,k);

  grid->cell_c[index] = matl->capacity*dx*dy*dz;

  grid->cell_gx[index] = matl->conductivity*dy*dz/dx;
  grid->cell_gy[index] = matl->conductivity*dx*dz/dy;
  grid->cell_gz[index] = matl->conductivity*dx*dy/dz;

  grid->cell_p[index] += pvol*dx*dy*dz;

  return;
}

/********************************************************************/
void grid_fill_block(struct grid *grid,struct block *block){

  int i,j,k,imin,imax,jmin,jmax,kmin,kmax;
  double vol,pvol;

  grid_map_block(grid,block,&imin,&jmin,&kmin,&imax,&jmax,&kmax);

  vol = axis_get_distance(grid->x,imin,imax)
       *axis_get_distance(grid->y,jmin,jmax)
       *axis_get_distance(grid->z,kmin,kmax);

  pvol = block->power*block->activity/vol;

  for(k=kmin; k<kmax; k++)
    for(j=jmin; j<jmax; j++)
      for(i=imin; i<imax; i++)
        grid_set_cell(grid,i,j,k,block->material,pvol);

  return;
}

/********************************************************************/
void grid_connect_cell(struct grid *grid,int i,int j,int k,
  double *gwe,double *gso,double *gbo,double *gsu){

  double dx,dy,dz,g,h,gea,gno,gto;
  int nx,ny,nz,ice,iwe,iea,iso,ino,ibo,ito;

  grid_get_dimension(grid,&nx,&ny,&nz);

  grid_get_cell_dimension(grid,i,j,k,&dx,&dy,&dz);
  
  ice = grid_get_cell_index(grid,i,j,k);
  iwe = grid_get_cell_index(grid,i-1,j,k);
  iea = grid_get_cell_index(grid,i+1,j,k);
  iso = grid_get_cell_index(grid,i,j-1,k);
  ino = grid_get_cell_index(grid,i,j+1,k);
  ibo = grid_get_cell_index(grid,i,j,k-1);
  ito = grid_get_cell_index(grid,i,j,k+1);

  g = grid->cell_gx[ice];

  if(i > 0)
    h = grid->cell_gx[iwe];
  else
    h = grid->h_west*dy*dz;

  *gwe = g*h/(g+h);

  if(i < nx-1)
    h = grid->cell_gx[iea]; 
  else
    h = grid->h_east*dy*dz;

  gea = g*h/(g+h);

  g = grid->cell_gy[ice];

  if(j > 0)
    h = grid->cell_gy[iso]; 
  else
    h = grid->h_south*dx*dz;

  *gso = g*h/(g+h);

  if(j < ny-1)
    h = grid->cell_gy[ino]; 
  else
    h = grid->h_north*dx*dz;

  gno = g*h/(g+h);

  g = grid->cell_gz[ice];

  if(k > 0)
    h = grid->cell_gz[ibo]; 
  else
    h = grid->h_bottom*dx*dy;

  *gbo = g*h/(g+h);

  if(k < nz-1)
    h = grid->cell_gz[ito]; 
  else
    h = grid->h_top*dx*dy;

  gto = g*h/(g+h);

  *gsu = *gwe + gea + *gso + gno + *gbo + gto;

  return;
}

/********************************************************************/
void grid_write_matrix_equation(struct grid *grid){

  int i,j,k,nx,ny,nz,n,row;
  double gwe,gso,gbo,gsu;

  n = grid_get_dimension(grid,&nx,&ny,&nz);

  grid->matrix_g = ssr_alloc(n,4*n);

  for(k = 0; k < nz; k++){
    for(j = 0; j < ny; j++){
      for(i = 0; i < nx; i++){

        grid_connect_cell(grid,i,j,k,&gwe,&gso,&gbo,&gsu);

        row = k*nx*ny+j*nx+i;

        ssr_set_value(grid->matrix_g,row,row,gsu);

        if(i > 0)
          ssr_set_value(grid->matrix_g,row,row-1,-gwe);

        if(j > 0)
          ssr_set_value(grid->matrix_g,row,row-nx,-gso);

        if(k > 0)
          ssr_set_value(grid->matrix_g,row,row-nx*ny,-gbo);
      }
    }
  }

  ssr_sort(grid->matrix_g);

  return;
}

/********************************************************************/
void grid_set_heat_transfer_ambient(struct grid *grid,struct model *model){

  grid->h_west = model->h_west;
  grid->h_east = model->h_east;
  grid->h_south = model->h_south;
  grid->h_north = model->h_north;
  grid->h_bottom = model->h_bottom;
  grid->h_top = model->h_top;

  return;
}

/********************************************************************/
struct grid *grid_mesh_model(struct model *model){

  struct grid *grid;
  int i;

  grid = grid_create();

  for(i = 0; i < model->block_count; i++)
    grid_mesh_block(grid,model->block[i]);

  grid_alloc_cell(grid);

  for(i = 0; i < model->block_count; i++)
    grid_fill_block(grid,model->block[i]);

  grid_set_heat_transfer_ambient(grid,model);

  grid_write_matrix_equation(grid);

  return grid;
}

/********************************************************************/
void grid_info(struct grid *grid){

  int nx,ny,nz,n;

  n = grid_get_dimension(grid,&nx,&ny,&nz);
  printf("Number of cells: %d x %d x %d = %d\n",nx,ny,nz,n);

  return;
}

/********************************************************************/
void grid_free(struct grid *grid){

  axis_free(grid->x);
  axis_free(grid->y);
  axis_free(grid->z);

  free(grid->cell_gx);
  free(grid->cell_gy);
  free(grid->cell_gz);
  free(grid->cell_c);
  free(grid->cell_p);
  free(grid->cell_t);

  return;
}

/********************************************************************/
void grid_output_block(struct grid *grid,struct block *block,FILE *file){

  int i,j,k,icell,imin,imax,jmin,jmax,kmin,kmax;
  double min,max,avg,capa,temp,sum;

  grid_map_block(grid,block,&imin,&jmin,&kmin,&imax,&jmax,&kmax);

  sum = 0;
  min = HUGE_VAL;
  max = 0;
  avg = 0;

  for(k=kmin; k<kmax; k++){
    for(j=jmin; j<jmax; j++){
      for(i=imin; i<imax; i++){

        icell = grid_get_cell_index(grid,i,j,k);

        temp = grid->cell_t[icell];
        capa = grid->cell_c[icell];

        min = fmin(min,temp);
        max = fmax(max,temp);

        avg += capa*temp; 
        sum += capa;

      }
    }
  }

  avg /= sum;

  fprintf(file,"%-10s %.3f  %.3f  %.3f\n",block->name,min,max,avg);

  return;
}

/********************************************************************/
void grid_output_model(struct grid *grid,struct model *model,char *path){

  int i;
  FILE *file;

  if(path)
    file = fopen(path,"w");
  else
    file = stdout;

  if(!file)
    return;

  fprintf(file,"%% name min max avg\n");
  for(i = 0; i < model->block_count; i++)
    grid_output_block(grid,model->block[i],file);

  fclose(file);
  return;
}
