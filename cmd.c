/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "cmd.h"

/********************************************************************/
void cmd_free(struct cmd *cmd){

  free(cmd);
  return;
}

/********************************************************************/
void cmd_fatal(struct cmd *cmd,char *format, ...){

  va_list args;

  fprintf(stderr,"Error: ");
  va_start(args,format);
  vfprintf(stderr,format,args);
  va_end(args);
  fprintf(stderr,"\n");

  cmd_print_usage(cmd);

  exit(-1);
}

/********************************************************************/
void cmd_print_usage(struct cmd *cmd){

  printf("Usage: %s [options] file\n",cmd->binary);
  printf("Options:\n");
  printf("  -o=<file>     Place the output into <file>\n");
  printf("  -v            Enable verbose output\n");
  printf("  -version      Display version information\n");

  return;
}

/********************************************************************/
void cmd_print_version(void){

  printf("********************************************************\n");
  printf("*                     ICTherm                          *\n");
  printf("********************************************************\n");
  printf("* Version: 1.0                                         *\n");
  printf("* Date: June 2014                                      *\n");
  printf("* Author: Alain Fourmigue (alain.fourmigue@polymtl.ca) *\n");
  printf("********************************************************\n");
  printf("This software is a trial version!\n");
  printf("If you have any question or would like to request\n");
  printf("additional features, please contact alain.fourmigue@polymtl.ca\n");
  
  exit(0);
  return;
}

/********************************************************************/
char *cmd_parse_option_path(struct cmd *cmd,char *option){

  char *path;

  path = strchr(option,'=');
  if(!path || strlen(path) < 2)
    cmd_fatal(cmd,"syntax error '%s'",option);

  return path+1;
}

/********************************************************************/
void cmd_parse_option(struct cmd *cmd,char *option){

  if(!strncmp(option,"-o",2))
    cmd->output = cmd_parse_option_path(cmd,option);

  else if(!strcmp(option,"-v"))
    cmd->verbose = 1;

  else if(!strcmp(option,"-version"))
    cmd_print_version();

  else 
    cmd_fatal(cmd,"unrecognized option '%s'",option);

  return;
}

/********************************************************************/
struct cmd *cmd_parse(char **argv,int argc){

  struct cmd *cmd;
  int i;

  cmd = calloc(1,sizeof(struct cmd));

  cmd->binary = argv[0];

  for(i = 1; i < argc; i++){

    if(*argv[i] == '-')
      cmd_parse_option(cmd,argv[i]);

    else
      cmd->input= argv[i];
  }
      
  if(!cmd->input)
    cmd_fatal(cmd,"no input file was specified");

  return cmd;
}


