/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#ifndef ssr_h
#define ssr_h

/********************************************************************/
struct  ssr {

  double *dval;
  double *lval;
  int *row;
  int *col;
  int nrow;
  int ncol;
  int nnz;
  int lnnz;
  int dnnz;

};

/********************************************************************/
struct ssr *ssr_alloc(int,int);
struct ssr *ssr_clone(struct ssr*);
void ssr_info(struct ssr*);
void ssr_print(struct ssr*);
void ssr_free(struct ssr*);
void ssr_mul_vec(struct ssr*,double*,double*);
void ssr_solve(struct ssr*,double*);
double ssr_get_value(struct ssr*,int,int);
void ssr_set_value(struct ssr*,int,int,double);
void ssr_sort(struct ssr*);


#endif


