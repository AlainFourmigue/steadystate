/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#ifndef cmd_h
#define cmd_h

/********************************************************************/
struct cmd {

  char *binary;
  char *input;
  char *output;
  int verbose;

};

/********************************************************************/
struct cmd *cmd_parse(char**,int);
void cmd_fatal(struct cmd*,char*,...);
void cmd_print_version(void);
void cmd_print_usage(struct cmd*);
void cmd_parse_option(struct cmd*,char*);
void cmd_free(struct cmd*);

/********************************************************************/
#endif


