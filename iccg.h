/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#ifndef iccg_h
#define iccg_h

#include "ssr.h"

/********************************************************************/
struct ssr *ichol(struct ssr*);
void iccg(struct ssr*,double*,double*,double);
double residual(struct ssr*,double*,double*);

/********************************************************************/

#endif


