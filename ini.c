/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <math.h>

#include "ini.h"

/********************************************************************/
char *ini_strip(char *buf){

  char *c;

  if((c=strchr(buf,'%'))!=NULL)
    *c=0;

  while(*buf && isspace(*buf))
    buf++;

  c=buf+strlen(buf)-1;
  while(c>=buf && isspace(*c))
    *c--=0;

  return buf;
}

/********************************************************************/
void ini_fatal(char *format, ...){

  va_list args;

  va_start(args,format);
  vfprintf(stderr,format,args);
  va_end(args);
  fprintf(stderr,"\n");

  exit(-1);
}

/********************************************************************/
void ini_fatal_key(struct key *key,char *format, ...){

  va_list args;

  fprintf(stderr,"%s:%d: ",key->path,key->line);
  va_start(args,format);
  vfprintf(stderr,format,args);
  va_end(args);
  fprintf(stderr,"\n");

  exit(-1);
}

/********************************************************************/
struct section *ini_create_section(char *path,int line,char *name){

  struct section *sec;

  sec = calloc(1,sizeof(struct section));
  sec->name = strdup(name);
  sec->line = line;
  sec->path = path;

  return sec;
}

/********************************************************************/
struct key *ini_create_key(char *path,int line,char *name,char *value){

  struct key *key;

  key = calloc(1,sizeof(struct key));
  
  key->name = strdup(name);
  key->value = strdup(value);
  key->line = line;
  key->path = path;

  return key;
}

/********************************************************************/
void ini_free_key(struct key *key){

  free(key->name);
  free(key->value);
  free(key);

  return;
}

/********************************************************************/
void ini_free_section(struct section *sec){

  int i;

  for(i=0; i<sec->key_count; i++)
    ini_free_key(sec->key[i]);

  free(sec->key);
  free(sec->name);
  free(sec);

  return;
}

/********************************************************************/
void ini_add_section(struct ini *ini,struct section *sec){

  ini->section_count++;
  ini->section = realloc(ini->section,
    ini->section_count*sizeof(struct section*));
  ini->section[ini->section_count-1] = sec;

  return;
}

/********************************************************************/
void ini_add_key(struct section *sec,struct key *key){

  sec->key_count++;
  sec->key = realloc(sec->key,sec->key_count*sizeof(struct key*));
  sec->key[sec->key_count-1] = key;

  return;
}

/********************************************************************/
void ini_read_section(struct ini *ini,char *buf,char *path,int line){

  char *name;
  struct section *sec;

  if(buf[0]!='[' || buf[strlen(buf)-1]!=']')
    ini_fatal("%s:%d: syntax error",path,line);

  buf[strlen(buf)-1]=0;
  buf++;
  name = ini_strip(buf);

  sec = ini_create_section(path,line,name);

  ini_add_section(ini,sec);

  return;
}

/********************************************************************/
void ini_read_key(struct ini *ini,char *buf,char *path,int line){

  char *name,*value;
  struct section *sec;
  struct key *key;

  value = strchr(buf,'=');
  *value = 0;
  value++;

  name = ini_strip(buf);
  value = ini_strip(value);

  if(!ini->section)
    ini_fatal("%s:%d: no section was declared prior to key '%s'",
      path,line,name);

  sec = ini->section[ini->section_count-1];

  key = ini_create_key(path,line,name,value);

  ini_add_key(sec,key);

  return;
}

/********************************************************************/
void ini_read_line(struct ini *ini,char *buf,char *path,int line){

  buf = ini_strip(buf);

  if(*buf==0)
    return;

  if(strchr(buf,'='))
    ini_read_key(ini,buf,path,line);

  else if(strchr(buf,'[') || strchr(buf,']'))
    ini_read_section(ini,buf,path,line);

  else
    ini_fatal("%s:%d: syntax error, no section nor key declared",path,line);

  return;
}

/********************************************************************/
struct section *ini_get_section(struct ini *ini,char *name){

  int i;

  for(i=0; i<ini->section_count; i++)
    if(!strcmp(ini->section[i]->name,name))
      return ini->section[i];

  return NULL;
}

/********************************************************************/
struct key *ini_get_key(struct section *sec,char *name){

  int i;

  for(i=0; i<sec->key_count; i++)
    if(!strcmp(sec->key[i]->name,name))
      return sec->key[i];

  return NULL;
}

/********************************************************************/
struct ini *ini_read_file(char *path){

  FILE *fd;
  char buf[1024];
  int line;
  struct ini *ini;

  fd = fopen(path,"r");
  if(!fd)
    ini_fatal("failed to open file '%s'",path);

  ini = calloc(1,sizeof(struct ini));
  ini->path = strdup(path);

  line = 1;
  while(fgets(buf,1024,fd))
    ini_read_line(ini,buf,path,line++);

  fclose(fd);

  return ini;
}

/********************************************************************/
void ini_info(struct ini *ini){

  printf("Number of sections: %d\n",ini->section_count);
  int i,j;
  struct section *sec;
  for(i=0; i<ini->section_count; i++){

    sec=ini->section[i];

    printf("section '%s'\n",sec->name);
    for(j=0; j<sec->key_count; j++)
      printf("  key '%s'\n",sec->key[j]->name);
  }
  return;
}

/********************************************************************/
void ini_free(struct ini *ini){

  int i;

  for(i=0; i<ini->section_count; i++)
    ini_free_section(ini->section[i]);

  free(ini->section);
  free(ini->path);
  free(ini);
  return;
}

/********************************************************************/
double ini_read_key_value(struct key *key,int flag){

  double value;
  char *end;

  value = strtod(key->value,&end);

  if((key->value == end) || (value < 0.0 && flag == 1) || 
    (value <= 0.0 && flag == 2))
    ini_fatal_key(key,"illegal value '%s'",key->value);

  return value;
}

