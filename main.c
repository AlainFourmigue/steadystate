/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#include <stdio.h>
#include <stdlib.h>

#include "model.h"
#include "grid.h"
#include "iccg.h"
#include "cmd.h"

#define MAX_CELL_COUNT 500000

/********************************************************************/
int main(int argc,char **argv){

  struct model *model;
  struct grid *grid;
  struct cmd *cmd;
  int n;

  cmd = cmd_parse(argv,argc);

  model = model_read_file(cmd->input);

  if(cmd->verbose)
    model_info(model);

  grid = grid_mesh_model(model);

  if(cmd->verbose)
    grid_info(grid);

  n = grid_get_dimension(grid,NULL,NULL,NULL);

  if(n > MAX_CELL_COUNT){

    fprintf(stderr,"******************************************************\n");
    fprintf(stderr,"*              This is not an error!                 *\n");
    fprintf(stderr,"* You required a resolution of more than %d cells *\n",
      MAX_CELL_COUNT);
    fprintf(stderr,"* which exceeds the limit of this trial version.     *\n");
    fprintf(stderr,"* Please contact the ICTherm team for additional     *\n");
    fprintf(stderr,"* feature support.                                   *\n");
    fprintf(stderr,"******************************************************\n");
    exit(-1);
  }


  printf("Computing steady state temperature...\n");
  iccg(grid->matrix_g,grid->cell_t,grid->cell_p,1e-9);
  printf("Finished!\n");

  grid_output_model(grid,model,cmd->output);

  grid_free(grid);
  model_free(model);
  cmd_free(cmd);

  return 0;
}


