/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#ifndef grid_h
#define grid_h

#include "axis.h"
#include "model.h"
#include "ssr.h"

/********************************************************************/
struct grid {

  struct axis *x,*y,*z;

  double h_west;
  double h_east;
  double h_south;
  double h_north;
  double h_bottom;
  double h_top;

  struct ssr *matrix_g;

  double *cell_p;
  double *cell_t;
  double *cell_c;
  double *cell_gx;
  double *cell_gy;
  double *cell_gz;

};

/********************************************************************/
struct grid *grid_mesh_model(struct model*);
void grid_info(struct grid*);
void grid_free(struct grid*);
void grid_output_model(struct grid*,struct model*,char*);
int grid_get_dimension(struct grid*,int*,int*,int*);

/********************************************************************/
#endif


