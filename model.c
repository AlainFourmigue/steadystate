/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "model.h"
#include "ini.h"

/********************************************************************/
void model_free(struct model *model){

  int i;

  for(i=0; i<model->block_count; i++)
    free(model->block[i]);
  free(model->block);

  for(i=0; i<model->material_count; i++)
    free(model->material[i]);
  free(model->material);

  free(model);

  return;
}

/********************************************************************/
void model_add_material(struct model *model,struct material *matl){

  model->material_count++;
  model->material = realloc(model->material,
    model->material_count*sizeof(struct material*));
  model->material[model->material_count-1] = matl;

  return;
}

/********************************************************************/
void model_add_block(struct model *model,struct block *block){

  model->block_count++;
  model->block = realloc(model->block,
    model->block_count*sizeof(struct block*));
  model->block[model->block_count-1] = block;

  return;
}

/********************************************************************/
struct material *model_get_material(struct model *model,char *name){

  int i;
  for(i=0; i<model->material_count; i++)
    if(!strcmp(model->material[i]->name,name))
      return model->material[i];

  return NULL;
}

/********************************************************************/
struct block *model_get_block(struct model *model,char *name){

  int i;
  for(i=0; i<model->block_count; i++)
    if(!strcmp(model->block[i]->name,name))
      return model->block[i];

  return NULL;
}

/********************************************************************/
void model_read_material(struct model *model,struct section *sec){

  struct material *matl;
  struct key *key;
  int i;

  matl = calloc(1,sizeof(struct material));
  model_add_material(model,matl);

  for(i=0; i<sec->key_count; i++){

    key = sec->key[i];

    if(!strcmp(key->name,"name"))
      matl->name = strdup(key->value);

    else if(!strcmp(key->name,"conductivity"))
      matl->conductivity = ini_read_key_value(key,2);

    else if(!strcmp(key->name,"capacity"))
      matl->capacity = ini_read_key_value(key,2);

    else if(!strcmp(key->name,"density"))
      matl->density = ini_read_key_value(key,2);

    else
      ini_fatal_key(key,"unexpected property '%s'",key->name);
  }

  if(!matl->name)
    ini_fatal("%s:%d: 'name' was not specified for this material",
      sec->path,sec->line);

  if(matl->conductivity <= 0.0)
    ini_fatal("%s: 'conductivity' was not specified for material '%s'",
      sec->path,matl->name);

  if(matl->capacity <= 0.0)
    ini_fatal("%s: 'capacity' was not specified for material '%s'",
      sec->path,matl->name);

  if(matl->density <= 0.0)
    ini_fatal("%s: 'density' was not specified for material '%s'",
      sec->path,matl->name);

  return;
}

/********************************************************************/
void model_read_component(struct model *model,struct section *sec){

  struct block *block,*parent;
  struct key *key;
  int i;

  block = calloc(1,sizeof(struct block));
  model_add_block(model,block);
  
  block->activity = -1;
  parent = NULL;

  for(i=0; i<sec->key_count; i++){

    key = sec->key[i];

    if(!strcmp(key->name,"name"))
      block->name = strdup(key->value);

    else if(!strcmp(key->name,"x"))
      block->x = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"y"))
      block->y = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"z"))
      block->z = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"length"))
      block->length = ini_read_key_value(key,2);

    else if(!strcmp(key->name,"width"))
      block->width = ini_read_key_value(key,2);

    else if(!strcmp(key->name,"height"))
      block->height = ini_read_key_value(key,2);

    else if(!strcmp(key->name,"power"))
      block->power = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"activity"))
      block->activity = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"resolution"))
      block->resolution = ini_read_key_value(key,2);

    else if(!strcmp(key->name,"material")){

      block->material = model_get_material(model,key->value);
      if(!block->material)
        ini_fatal_key(key,"undefined material '%s'",key->value);
    } 
    else if(!strcmp(key->name,"parent")){

      parent = model_get_block(model,key->value);
      if(!parent)
        ini_fatal_key(key,"undefined component '%s'",key->value);
    } 
    else 
      ini_fatal_key(key,"unexpected property '%s'",key->name);
  }

  if(!block->name)
    ini_fatal("%s:%d: 'name' was not specified for this component",
      sec->path,sec->line);

  if(!parent)
    ini_fatal("%s: 'parent' was not specified for component '%s",
      sec->path,block->name);

  block->x += parent->x;
  block->y += parent->y;
  block->z += parent->z;

  if(block->length <= 0.0)
    block->length = parent->length;

  if(block->width <= 0.0)
    block->width = parent->width;

  if(block->height <= 0.0)
    block->height = parent->height;

  if(!block->material)
    block->material = parent->material;

  if(block->resolution <= 0.0)
    block->resolution = parent->resolution;

  if(block->activity <= 0.0)
    block->activity = parent->activity;

  if(block->x + block->length > parent->x + parent->length ||
     block->y + block->width > parent->y + parent->width   ||
     block->z + block->height > parent->z + parent->height )
    ini_fatal("%s: component '%s' is too large",sec->path,block->name);

  return;
}

/********************************************************************/
void model_read_device(struct model *model,struct section *sec){

  struct block *block;
  struct key *key;
  int i;

  block = calloc(1,sizeof(struct block));
  model_add_block(model,block);
  
  block->activity = 1; // default activity

  for(i=0; i<sec->key_count; i++){

    key = sec->key[i];

    if(!strcmp(key->name,"name"))
      block->name = strdup(key->value);

    else if(!strcmp(key->name,"length"))
      block->length = ini_read_key_value(key,2);

    else if(!strcmp(key->name,"width"))
      block->width = ini_read_key_value(key,2);

    else if(!strcmp(key->name,"height"))
      block->height = ini_read_key_value(key,2);

    else if(!strcmp(key->name,"power"))
      block->power = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"activity"))
      block->activity = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"resolution"))
      block->resolution = ini_read_key_value(key,2);

    else if(!strcmp(key->name,"west"))
      model->h_west = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"east"))
      model->h_east = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"south"))
      model->h_south = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"north"))
      model->h_north = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"bottom"))
      model->h_bottom = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"top"))
      model->h_top = ini_read_key_value(key,1);

    else if(!strcmp(key->name,"material")){

      block->material = model_get_material(model,key->value);
      if(!block->material)
        ini_fatal_key(key,"undefined material '%s'",key->value);
    } 
    else 
      ini_fatal_key(key,"unexpected property '%s'",key->name);
  }

  if(!block->name)
    ini_fatal("%s:%d: 'name' was not specified for this device",
      sec->path,sec->line);

  if(block->length <= 0.0)
    ini_fatal("%s: 'length' was not specified for device '%s'",
      sec->path,block->name);

  if(block->width <= 0.0)
    ini_fatal("%s: 'width' was not specified for device '%s'",
      sec->path,block->name);

  if(block->height <= 0.0)
    ini_fatal("%s: 'height' was not specified for device '%s'",
      sec->path,block->name);

  if(!block->material)
    ini_fatal("%s: 'material' was not specified for device '%s'",
      sec->path,block->name);

  if(block->resolution <= 0.0)
    block->resolution = 400e-6; // default resolution

  if(model->h_west <= 0.0 &&
     model->h_east <= 0.0 &&
     model->h_south <= 0.0 &&
     model->h_north <= 0.0 &&
     model->h_bottom <= 0.0 &&
     model->h_top <= 0.0 )
    ini_fatal("%s: at leat one heat transfer coefficient must be non null",
      sec->path);

  return;
}

/********************************************************************/
struct model *model_read_file(char *path){

  struct model *model;
  struct ini *ini;
  struct section *sec;
  int i;

  model = calloc(1,sizeof(struct model));

  ini = ini_read_file(path);

  for(i=0; i<ini->section_count; i++)
    if(!strcmp(ini->section[i]->name,"material"))
      model_read_material(model,ini->section[i]);

  sec = ini_get_section(ini,"device");
  if(!sec)
    ini_fatal("%s: missing section 'device'",ini->path);

  model_read_device(model,sec);

  for(i=0; i<ini->section_count; i++)
    if(!strcmp(ini->section[i]->name,"component"))
      model_read_component(model,ini->section[i]);

  ini_free(ini);

  return model;
}

/********************************************************************/
void model_info(struct model *model){

  struct block *block;
  int i;

  printf("Number of materials: %d\n",model->material_count);
  printf("Number of blocks: %d\n",model->block_count);

  for(i = 0; i < model->block_count; i++){

    block = model->block[i];

    printf("Block '%s'\n",block->name);
    printf("  Position (mm): %.3f,%.3f,%.3f\n",
      block->x*1e3,
      block->y*1e3,
      block->z*1e3);
    printf("  Dimension (mm): %.3f,%.3f,%.3f\n",
      block->length*1e3,
      block->width*1e3,
      block->height*1e3);
    printf("  Power (W): %.3f W\n",block->power);
    printf("  Activity: %.2f\n",block->activity);
  }

  return;
}
