CC=gcc

CFLAGS= -g -Wall 
CFLAGS+= -O2 -funroll-loops

LDLIBS= -lm 

EXEC= ictherm 

all: $(EXEC)

OBJ= main.o
OBJ += cmd.o
OBJ += ini.o
OBJ += model.o
OBJ += axis.o
OBJ += grid.o
OBJ += ssr.o
OBJ += iccg.o

$(EXEC): $(OBJ) 
	$(CC) -o $@ $^ $(LDFAGS) $(LDLIBS)  

clean: 
	$(RM) $(EXEC) $(OBJ)

DIR:=$(shell basename `pwd`)

DATE:=$(shell date +%d%b)

tar: clean
	(cd ..; tar -cf $(DIR)_$(DATE).tar $(DIR))

