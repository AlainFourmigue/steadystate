/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#ifndef model_h
#define model_h

/********************************************************************/
struct material {

  char *name;
  double conductivity;
  double capacity;
  double density;

};

/********************************************************************/
struct block {

  char *name;
  double x,y,z;
  double length,width,height;
  double power;
  double activity;
  double resolution;
  struct material *material;

};

/********************************************************************/
struct model {

  struct material **material;
  int material_count;

  struct block **block;
  int block_count;

  double h_west;
  double h_east;
  double h_south;
  double h_north;
  double h_top;
  double h_bottom;

};

/********************************************************************/
struct model *model_read_file(char*);
void model_info(struct model*);
void model_free(struct model*);

struct block *model_create_block(struct model*,char*);
struct material *model_create_material(struct model*,char*);

struct block *model_get_block(struct model*,char*);
struct material *model_get_material(struct model*,char*);

void model_add_material(struct model*,struct material*);
void model_add_block(struct model*,struct block*);

/********************************************************************/

#endif


