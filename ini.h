/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#ifndef ini_h
#define ini_h

/********************************************************************/
struct key {

  char *name;
  char *value;
  int line;
  char *path;

};

/********************************************************************/
struct section {

  struct key **key;
  int key_count;
  char *name;
  int line;
  char *path;
  void *data;

};

/********************************************************************/
struct ini {

  struct section **section;
  int section_count;
  char *path;

};

/********************************************************************/
struct ini *ini_read_file(char*);
void ini_info(struct ini*);
void ini_free(struct ini*);

struct key *ini_get_key(struct section*,char*);
struct section *ini_get_section(struct ini*,char*);

void ini_fatal(char *,...);
void ini_fatal_key(struct key*,char*,...);
double ini_read_key_value(struct key*,int);

/********************************************************************/
#endif


