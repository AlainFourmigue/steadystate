/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "iccg.h"

/*******************************************************************/
static void mov(double *u,double *v,int n){

  int i;
  for(i=0; i<n; i++)
    u[i]=v[i];  
  return;
}

/*******************************************************************/
static double dot(double *u,double *v,int n){

  int i;
  double sum=0;
  for(i=0; i<n; i++)
    sum+=u[i]*v[i];  
  return sum;
}

/*******************************************************************/
static void mac(double *a,double *b,double f,double *c,int n){

  int i;
  for(i=0; i<n; i++)
    a[i]=b[i]+f*c[i];  
  return;
}

/*******************************************************************/
static double mac_norm(double *r,double f,double *z,int n){

  int i;
  double sum=0;
  for(i=0; i<n; i++){
    r[i]+=f*z[i];  
    z[i]=r[i];
    sum+=fabs(z[i]);
  }
  return sum;
}

/*******************************************************************/ 
static void set(double *x,double v,int n){ 
 
  int i; 
  for(i=0; i<n; i++) 
    x[i]=v;
  return;
} 

/*******************************************************************/
static double norm1(double *u,int n){

  int i;
  double sum=0;
  for(i=0; i<n; i++)
    sum+=fabs(u[i]);
  return sum;
}

/********************************************************************/
struct ssr *ichol(struct ssr *A){

  int i,j,k;
  double coef;
  struct ssr *L;

  L=ssr_clone(A);

  for(i=1; i<L->nrow; i++){

    for(j=L->row[i]; j<L->row[i+1]; j++){

      for(k=L->row[A->col[j]]; k<L->row[A->col[j]+1]; k++){
        
        L->lval[j]-=L->lval[k]*ssr_get_value(L,i,L->col[k]);
      }
    }

    for(j=L->row[i]; j<L->row[i+1]; j++){
      
      coef=L->lval[j]/L->dval[L->col[j]];
      L->dval[i]-=coef*L->lval[j];
      L->lval[j]=coef;
    }
  }

  for(i=0; i<L->nrow; i++)
    L->dval[i]=1/L->dval[i];

  return L;
}

/********************************************************************/
void iccg(struct ssr *A,double *x,double *b,double tol){

  struct ssr *L;
  double *z,*r,*s;
  double alpha,beta,res,cur,prev;
  int i=0,n;

  n=A->ncol;

  set(x,0,n);

  if(norm1(b,n)<tol) // check for null vector
    return;

  L = ichol(A);

 // printf("Initial residual: %.3e\n",norm1(b,n));

  r=(double*)calloc(n,sizeof(double));
  z=(double*)calloc(n,sizeof(double));
  s=(double*)calloc(n,sizeof(double));

  mov(r,b,n);

  mov(z,r,n);

  ssr_solve(L,z); // apply preconditioner LDL* z = b

  prev=dot(z,r,n);

  mov(s,z,n); // s = z

  ssr_mul_vec(A,s,z); // z <- A * s

  alpha=prev/dot(z,s,n);

  mac(x,x,alpha,s,n);

  res=mac_norm(r,-alpha,z,n);

  while(res>tol){

    ssr_solve(L,z); // apply preconditioner LDL* z = b

    cur=dot(z,r,n);

    beta=cur/prev;

    mac(s,z,beta,s,n); // s = z + beta * s

    ssr_mul_vec(A,s,z); // z <- A * s

    alpha=cur/dot(z,s,n);

    mac(x,x,alpha,s,n);

    res=mac_norm(r,-alpha,z,n);

//    printf("iter %d: res=%.5e\n",i,res);
    if(i > 10000){
      fprintf(stderr,"error: failed to solve the thermal problem...\n");
      exit(-1);
    }

    prev=cur; // save curent dot for the next iteration

    i++;
  }

  free(z);
  free(r);
  free(s);

  ssr_free(L);
 
  return;
}

/********************************************************************/
double residual(struct ssr *A,double *x,double *b){

  int i,n;
  double res,*y;

  n=A->ncol;

  y=(double*)calloc(n,sizeof(double));

  ssr_mul_vec(A,x,y);

  for(i=0; i<n; i++)
    y[i]-=b[i];

  res=norm1(y,n);

  free(y);

  return res;
}
