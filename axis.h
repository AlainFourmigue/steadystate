/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#ifndef axis_h
#define axis_h

/********************************************************************/
struct axis {

  char *name;
  double *tick;
  int tick_count;

};

/********************************************************************/
struct axis *axis_create(char*);
void axis_free(struct axis*);
void axis_add_tick(struct axis*,double,double);
int axis_get_tick(struct axis*,double,double);
int axis_get_closest_tick(struct axis*,double);
void axis_print(struct axis*);
void axis_mesh_interval(struct axis*,double,double,double);
double axis_get_interval(struct axis*,int);
double axis_get_distance(struct axis*,int,int);
int axis_count_interval(struct axis*);

/********************************************************************/
#endif


