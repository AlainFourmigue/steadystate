/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ssr.h"

/********************************************************************/
void ssr_mul_vec(struct ssr *A,double *x,double *y){

  int i,j;

  for(i=0; i<A->nrow; i++){

    y[i]=A->dval[i]*x[i];

    for(j=A->row[i]; j<A->row[i+1]; j++)
      y[i]+=A->lval[j]*x[A->col[j]];

    for(j=A->row[i]; j<A->row[i+1]; j++)
      y[A->col[j]]+=A->lval[j]*x[i];
  }

  return;
}

/********************************************************************/
void ssr_solve(struct ssr *L,double *x){

  int i,j;

  /* forward substitution */
  for(i=1; i<L->nrow; i++)
    for(j=L->row[i]; j<L->row[i+1]; j++)
      x[i]-=L->lval[j]*x[L->col[j]];

  for(i=0; i<L->nrow; i++)
    x[i]*=L->dval[i];

  /* backward substitution */
  for(i=L->nrow-1; i>=0; i--)
    for(j=L->row[i]; j<L->row[i+1]; j++)
      x[L->col[j]]-=L->lval[j]*x[i];

  return;
}

/********************************************************************/
void ssr_info(struct ssr *A){

  printf("nrow: %d\n",A->nrow);
  printf("ncol: %d\n",A->ncol);
  printf("dnnz: %d\n",A->dnnz);
  printf("lnnz: %d\n",A->lnnz);
  printf("nnz: %d\n",A->nnz);

  return;
}

/********************************************************************/
void ssr_free(struct ssr *A){

  free(A->dval);
  free(A->lval);
  free(A->row);
  free(A->col);
  free(A);

  return;
}

/********************************************************************/
 double ssr_get_value(struct ssr *A,int i,int j){

  int k;

  if(i==j)
    return A->dval[i];

  if(i<j)
    return ssr_get_value(A,j,i);

  for(k=A->row[i]; k<A->row[i+1]; k++)
    if(A->col[k]==j)
      return A->lval[k];

  return 0.0;
}

/********************************************************************/
void ssr_print(struct ssr *A){

  int i,j;
  double val;
  for(i=0; i<A->nrow && i<15; i++){
    for(j=0; j<A->ncol && j<15; j++){
      val=ssr_get_value(A,i,j);
      if(fabs(val)>1e-13)
        printf("% .2e ",val);
      else
        printf(" .        ");
    }
    printf("\n");
  }
  printf("\n");
  return;
}

/********************************************************************/
struct ssr *ssr_alloc(int nrow,int nzmax){

  struct ssr *A;

  A = calloc(1,sizeof(struct ssr));

  A->nrow = nrow;
  A->ncol = nrow;

  A->lval = calloc(nzmax-nrow,sizeof(double));
  A->dval = calloc(nrow,sizeof(double));
  A->row = calloc(nrow+1,sizeof(int));
  A->col = calloc(nzmax-nrow,sizeof(int));

  return A;
}

/********************************************************************/
struct ssr *ssr_clone(struct ssr *A){
    
  struct ssr *L;
  int i,j;

  L = ssr_alloc(A->nrow,A->nnz);

  for(i=0; i<A->nrow; i++){

    L->row[i]=A->row[i];
    L->dval[i]=A->dval[i];

    for(j=A->row[i]; j<A->row[i+1]; j++){
      L->col[j]=A->col[j];
      L->lval[j]=A->lval[j];
    }
  }
  L->row[i]=A->row[i];

  return L;
}

/********************************************************************/
void ssr_sort(struct ssr *A){

  int i,j,col,swap;
  double val;

  for(i=0; i<A->nrow; i++){

    swap = 1;
    while(swap){

      swap=0;
      for(j=A->row[i]; j<A->row[i+1]-1; j++){

        if(A->col[j]>A->col[j+1]){

          col=A->col[j];
          A->col[j]=A->col[j+1];
          A->col[j+1]=col;

          val=A->lval[j];
          A->lval[j]=A->lval[j+1];
          A->lval[j+1]=val;

          swap=1;
        }
      }
    }
  }
  return;
}

/********************************************************************/
void ssr_set_value(struct ssr *A,int row,int col,double val){

  if(row == col){

    A->dval[row] = val;
    A->dnnz++;
  }
  else {

    A->lval[A->lnnz] = val;
    A->col[A->lnnz] = col;
    A->lnnz++;
    A->row[row+1] = A->lnnz;
  }

  A->nnz = A->dnnz + A->lnnz;

  return;
}


