/* Copyright (c) 2014 - Alain Fourmigue - All Rights Reserved */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>

#include "axis.h"

/********************************************************************/
void axis_error(struct axis *axis,char *format, ...){

  va_list args;

  fprintf(stderr,"error: axis '%s': ",axis->name);
  va_start(args,format);
  vfprintf(stderr,format,args);
  va_end(args);
  fprintf(stderr,"\n");

  exit(-1);
}

/********************************************************************/
struct axis *axis_create(char *name){

  struct axis *axis;
  axis = calloc(1,sizeof(struct axis));
  axis->name = strdup(name);
  return axis;
}

/********************************************************************/
void axis_free(struct axis *axis){

  free(axis->name);
  free(axis->tick);
  free(axis);
  return;
}

/********************************************************************/
int axis_get_tick(struct axis *axis,double pos,double tol){

  int i;

  for(i = 0; i < axis->tick_count; i++)
    if(fabs(axis->tick[i]-pos) < tol)
      return i;

  return -1;
}

/********************************************************************/
int axis_get_closest_tick(struct axis *axis,double pos){

  int i,imin;
  double min,dist;

  imin = -1;
  min = HUGE_VAL;

  for(i=0; i<axis->tick_count; i++){

    dist = fabs(axis->tick[i]-pos);

    if(dist < min){
      min = dist;
      imin = i;
    }
  }
  return imin;
}

/********************************************************************/
void axis_add_tick(struct axis *axis,double tick,double res){

  int i;

  if(axis_get_tick(axis,tick,res) >= 0)
    return;

  axis->tick_count++;
  axis->tick = realloc(axis->tick,axis->tick_count*sizeof(double));

  for(i = axis->tick_count-1; i > 0; i--){
    if(axis->tick[i-1] < tick)
      break;
    axis->tick[i] = axis->tick[i-1];
  }
  axis->tick[i] = tick;

  return;
}

/********************************************************************/
double axis_get_interval(struct axis *axis,int i){

  if(i < 0 || i >= axis->tick_count-1)
    axis_error(axis,"interval '%d' is out of range",i);

  return axis->tick[i+1] - axis->tick[i];
}

/********************************************************************/
double axis_get_distance(struct axis *axis,int imin,int imax){

  if(imin < 0 || imin >= axis->tick_count)
    axis_error(axis,"no such tick '%d'",imin);

  if(imax < 0 || imax >= axis->tick_count)
    axis_error(axis,"no such tick '%d'",imax);

  return fabs(axis->tick[imax]-axis->tick[imin]);
}

/********************************************************************/
int axis_count_interval(struct axis *axis){
  
  if(axis->tick_count < 1)
    return 0;

  return axis->tick_count-1;
}

/********************************************************************/
void axis_mesh_interval(struct axis *axis,double pos,double len,double step){

  double tol,lim;

  tol = 0.01*len;
  lim = pos+len;

  axis_add_tick(axis,pos,tol);
  axis_add_tick(axis,lim,tol);

  pos += step;

  while(pos < lim){

    axis_add_tick(axis,pos,step/2);
    pos += step;
  }

  return;
}

/********************************************************************/
void axis_print(struct axis *axis){

  int i;
  printf("%s: %d ticks\n",axis->name,axis->tick_count);
  for(i=0; i<axis->tick_count; i++){
    printf("%.3e ",axis->tick[i]);
    if(i%10==9)
      printf("\n");
  }
  printf("\n");
  return;
}


